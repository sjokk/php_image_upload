<?php

session_start();

ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
error_reporting(-1);

require __DIR__ . '/vendor/autoload.php';

use Images\ImagesRoutes;
use Images\Generics\EntryPoint;

$route = $_GET['route'] ?? '';
$entryPoint = new EntryPoint($route, new ImagesRoutes());
$entryPoint->run();