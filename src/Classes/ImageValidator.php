<?php

namespace Images\Classes;

use Images\Generics\DangerAlert;
use Images\Generics\SuccessAlert;

class ImageValidator extends AbstractValidator
{
    private $imageInformation;
    private $store;

    public function __construct(array $formData, array $imageInformation, bool $store)
    {
        $this->store = $store;
        $this->imageInformation = $imageInformation;
        parent::__construct($formData);
    }

    public function extract(array $formData): array
    {
        $extractedData = [
            ':title' => $formData['title'] ?? '',
            ':hidden' => isset($formData['hidden']) ? 1 : 0,
        ];
        
        if (!$this->store) {
            $extractedData[':id'] = $formData['id'];
        }

        return $extractedData;
    }

    public function validate(): void
    {
        $this->required(':title', 'Title');

        if ($this->store) {
            $this->imageRequired();
            $this->errorFreeImage();
            $this->storeImage();
        }

        if ($this->isValid()) {
            $method = $this->store ? "uploaded" : "changed";
            $message = "Image successfully $method.";
            $this->messages[] = new SuccessAlert($message);
        }
    }

    private function imageRequired()
    {
        if ($this->imageInformation['size'] <= 0) {
            $this->required('image', 'Image');
        }
    }

    private function errorFreeImage()
    {
        if ($this->imageInformation['error'] > 0) {
            $this->messages[] = new DangerAlert('Could not upload the selected image.');
            $this->valid = false;
        }
    }
    
    private function storeImage()
    {
        if ($this->isValid()) {
            $imageManager = new ImageManager('uploads');
            $this->formData[':image'] = $imageManager->storeOnDisk($this->imageInformation);
        }
    }

    public function getInput()
    {
        return $this->formData;
    }
}