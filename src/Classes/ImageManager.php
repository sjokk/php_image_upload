<?php

namespace Images\Classes;

class ImageManager
{
    private $uploadFolder;

    public function __construct($uploadFolder)
    {
        $this->uploadFolder = $uploadFolder;
    }

    public function storeOnDisk(array $imageInformation): string
    {
        $uploadPath = __DIR__ . "/../../{$this->uploadFolder}";

        $newName = md5(uniqid(rand(), true));
        
        $path_parts = pathinfo($imageInformation["name"]);
        $extension = $path_parts['extension'];
        
        $fileName = "$newName.$extension";
        $imageLocation = "$uploadPath/$fileName";
        $imageRelativeLocation = "{$this->uploadFolder}/$fileName";

        move_uploaded_file($imageInformation['tmp_name'], $imageLocation);

        return $imageRelativeLocation;
    }
}