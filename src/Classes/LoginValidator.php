<?php

namespace Images\Classes;

use Images\Generics\DangerAlert;

class LoginValidator extends AbstractValidator
{

    public function extract(array $formData): array
    {
        return [
            'username' => $formData['username'] ?? '',
            'password' => $formData['password'] ?? ''
        ];
    }
    
    public function validate(): void
    {
        $this->required('username', 'Username');
        $this->required('password', 'Password');

        if ($this->isValid()) {
            $this->validateCredentials();
        }
    }

    private function validateCredentials()
    {
        if (!$this->isMaster()) {
            $this->messages[] = new DangerAlert('Invalid credentials supplied.');
            $this->valid = false;
        }
    }

    private function isMaster(): bool
    {
        return $this->formData['username'] === getenv('app_user') 
            && $this->formData['password'] === getenv('app_user_password');
    }
}