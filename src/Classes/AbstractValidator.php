<?php

namespace Images\Classes;

use Images\Generics\DangerAlert;

abstract class AbstractValidator
{
    protected $formData;

    protected $valid = true;
    protected $messages = [];

    public function __construct(array $formData)
    {
        $this->formData = $this->extract($formData);
        $this->validate();
    }

    abstract function extract(array $formData): array;
    abstract function validate(): void;

    protected function required(string $inputField, string $humanReadableFieldName)
    {
        if (empty($this->formData[$inputField])) {
            $this->messages[] = new DangerAlert("$humanReadableFieldName is required.");
            $this->valid = false;
        }
    }

    public function getMessages()
    {
        return $this->messages;
    }

    public function isValid(): bool
    {
        return $this->valid;
    }
}