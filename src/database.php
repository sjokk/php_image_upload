<?php

function getConnection()
{
    return new PDO('mysql:host=localhost;dbname=IMAGES;', 'dev', 'password1234');
}

function store($table, $fieldValueArray)
{
    $sql = "INSERT INTO `$table` (";
    $columns = '';
    $values = '';
    $update = '';
    
    foreach ($fieldValueArray as $field => $value) {
        $column = ltrim($field, ':');
        $columns .= "`$column`,";
        $values .= "$field,";
        $update .= "$column=$field,";
    }
    
    $sql .= rtrim($columns, ',') . ') VALUES (' . rtrim($values, ',') . ') ON DUPLICATE KEY UPDATE ' . rtrim($update, ',') . ';';
    getConnection()->prepare($sql)->execute($fieldValueArray);
}

function delete($table, $id)
{
    $sql = "UPDATE $table SET deleted = 1 WHERE id = ?";
    getConnection()->prepare($sql)->execute([$id]);
}

function get($table, $id)
{
    $stmt = getQuery($table, " WHERE id = $id;");

    return $stmt->fetch();
}

function getAll($table, $additionalSortingRules = ';')
{
    $stmt = getQuery($table, $additionalSortingRules);
    
    return $stmt->fetchAll();
}

function getQuery($table, $additionalSortingRules = ';')
{
    $sql = "SELECT * FROM `$table`";
    $sql .= $additionalSortingRules;
    $stmt = getConnection()->query($sql);

    return $stmt;
}

function getNeighboringImages($table, $id)
{
    // Since we work in reversed order, newest first.
    $neighboringImages['next'] = getNext($table, $id);
    $neighboringImages['previous'] = getPrevious($table, $id);
    
    return $neighboringImages;
}

function verify($table, $id)
{
    $sql = "UPDATE $table SET verified = 1 WHERE id = ?";
    
    return getConnection()->prepare($sql)->execute([$id]);
}

function getNext($table, $id, $next = true)
{
    $logicalOperator = '<';
    $aggregateFunction = 'MAX';

    if (!$next) {
        $logicalOperator = '>';
        $aggregateFunction = 'MIN';
    }

    $stmt = getQuery($table, " WHERE id $logicalOperator $id AND id = (SELECT $aggregateFunction(id) FROM $table WHERE id $logicalOperator $id);");

    return $stmt->fetch();
}

function getPrevious($table, $id)
{
    return getNext($table, $id, $next = false);
}