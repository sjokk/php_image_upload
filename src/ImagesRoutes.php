<?php

namespace Images;

use Images\Controllers\ImageController;
use Images\Generics\Routes;

class ImagesRoutes implements Routes
{
    const HOME = 'images';
    const ROUTES = [
        self::HOME => [
            'GET' => [
                'controller' => 'ImageController',
                'action' => 'sfw',
            ]
        ],
        self::HOME . '/nsfw' => [
            'GET' => [
                'controller' => 'ImageController',
                'action' => 'nsfw',
            ]
        ],
        self::HOME . '/create' => [
            'GET' => [
                'controller' => 'ImageController',
                'action' => 'show',
            ],
            'POST' => [
                'controller' => 'ImageController',
                'action' => 'store',
            ],
        ],        
        self::HOME . '/edit' => [
            'GET' => [
                'controller' => 'ImageController',
                'action' => 'edit',
            ],
            'POST' => [
                'controller' => 'ImageController',
                'action' => 'store',
            ],
        ],
        self::HOME . '/view' => [
            'GET' => [
                'controller' => 'ImageController',
                'action' => 'view',
            ]
        ],
        self::HOME . '/delete' => [
            'POST' => [
                'controller' => 'ImageController',
                'action' => 'delete',
            ],
        ],
        self::HOME . '/deleted' => [
            'GET' => [
                'controller' => 'ImageController',
                'action' => 'deleted',
            ],
        ],
        self::HOME . '/verify' => [
            'GET' => [
                'controller' => 'ImageController',
                'action' => 'unverified',
            ],
            'POST' => [
                'controller' => 'ImageController',
                'action' => 'verify',
            ],
        ],
        self::HOME . '/login' => [
            'GET' => [
                'controller' => 'LoginController',
                'action' => 'show',
            ],
            'POST' => [
                'controller' => 'LoginController',
                'action' => 'validate',
            ],
        ],
        self::HOME . '/logout' => [
            'GET' => [
                'controller' => 'LoginController',
                'action' => 'logout',
            ],
        ],
    ];

    public function callAction(string $route)
    {
        $requestMethod = $_SERVER['REQUEST_METHOD'];
        if (isset(self::ROUTES[$route][$requestMethod])) {
            switch ($route) {
                case self::HOME . '/nsfw':
                case self::HOME . '/verify':
                case self::HOME . '/deleted':
                case self::HOME . '/edit':
                case self::HOME . '/delete':
                    if (!isMaster()) {
                        $_SESSION['previous_url'] = $_SERVER['REQUEST_URI'];
                        header('Location: ?route=' . self::HOME . '/login');
                    }
            }
            $requested = self::ROUTES[$route][$requestMethod];
            $controller = "Images\Controllers\\" . $requested['controller'];
            $controller = new $controller();
            $action = $requested['action'];
            $page =  $controller->$action();
            if (isMaster()) {
                $controller->checkForNewUpload();
            }
            return $page;
        } else {
            http_response_code(404);
            header('Location: ?route=' . self::HOME);
        }
    }

    public function getHome(): string
    {
        return self::HOME;
    }

    public function getRoutes(): array
    {
        return self::ROUTES;
    }
}