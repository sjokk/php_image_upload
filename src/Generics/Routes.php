<?php
namespace Images\Generics;

interface Routes
{
    public function getHome(): string;
    public function getRoutes(): array;
}