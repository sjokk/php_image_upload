<?php

namespace Images\Generics;

abstract class AbstractAlert
{
    private $message;
    private $type;

    public function __construct(string $type, string $message)
    {
        $this->type = $type;
        $this->message = $message;
    }

    public function output()
    {
        ob_start();
        include __DIR__ . '/../../templates/alert.html.php';
        return ob_get_clean();
    }
}