<?php

namespace Images\Generics;

use Images\Generics\AbstractAlert;

class SuccessAlert extends AbstractAlert
{
    public function __construct(string $message)
    {
        parent::__construct('success', $message);
    }
}