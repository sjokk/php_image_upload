<?php

namespace Images\Generics;

use Images\Generics\AbstractAlert;

class DangerAlert extends AbstractAlert
{
    public function __construct(string $message)
    {
        parent::__construct('danger', $message);
    }
}