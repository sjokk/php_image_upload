<?php

function dd()
{
    $args = func_get_args();
    foreach($args as $arg) {
        var_dump($arg);
        echo '<br/>';
        echo '<br/>';
    }
    die(1);
}

function isMaster()
{
    return isset($_SESSION['master']);
}