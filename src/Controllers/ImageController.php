<?php

namespace Images\Controllers;

use Images\Classes\ImageValidator;
use Images\Generics\DangerAlert;
use Images\Generics\SuccessAlert;

class ImageController
{
    const ORIGINAL_TABLE = 'image_uploads';
    const PUBLIC_VIEW = 'sfw_image_uploads';

    public function index($title)
    {
        $table = $_SESSION['table'];
        $images = getAll($table, ' ORDER BY uploaded_at DESC;');

        return [
            'title' => $title,
            'template' => 'image-overview.html.php',
            'variables' => [
                'images' => $images,
            ],
        ];
    }

    public function sfw()
    {
        $title = 'Uploaded SFW Images';
        $_SESSION['table'] = 'sfw_image_uploads';
        
        return $this->index($title);
    }

    public function nsfw()
    {
        if (isMaster()) {
            $title = 'Uploaded NSFW Images';
            $_SESSION['table'] = 'nsfw_images';
            
            return $this->index($title);
        }
    }

    public function checkForNewUpload()
    {
        $table = 'unverified_images';
        $image = getAll($table);
        $_SESSION['new_upload'] = empty($image) ? false : true;
    }

    public function deleted()
    {
        if (isMaster()) {
            $title = 'Deleted Images';
            $_SESSION['table'] =  'deleted_images';
            
            return $this->index($title);
        }
    }

    public function verify()
    {
        if (isMaster()) {
            $id = $_POST['id'];
            $table = self::ORIGINAL_TABLE;
            verify($table, $id);
            $next = $_POST['next'];
            $route = !empty($next) ? "?route=images/view&id=$next" : '?route=images/verify';
            header("Location: $route");
        }
    }

    public function unverified()
    {
        if (isMaster()) {
            $title = 'Unverified Images';
            $_SESSION['table'] = 'unverified_images';
            
            return $this->index($title);
        }
    }

    public function view($titlePrefix = 'View', $template = 'image-view.html.php')
    {
        $id = $_GET['id'];
        $table = $_SESSION['table'];

        $image = get($table, $id);

        if(empty($image)) {
            $title = "404 - $id not found";
            $template = '404.html.php';
        } else {
            $title = "$titlePrefix: ($id) - {$image['title']}";
            $variables = [
                'id' => $id,
                'image' => $image,
                'imageLocation' => $image['image'],
                'next' => getNext($table, $id),
                'previous' => getPrevious($table, $id),
            ];
        }

        return [
            'title' => $title,
            'template' => $template,
            'variables' => isset($variables) ? $variables : []
        ];
    }

    public function edit()
    {
        if (isMaster()) {
            return $this->view('Edit', 'image-form.html.php');
        }
    }
    
    public function delete()
    {
        if (isMaster()) {
            $id = $_POST['id'];
            delete(self::ORIGINAL_TABLE, $id);
            header('Location: ?route=images/deleted');
        }
    }

    public function show()
    {
        return [
            'title' => 'Image Upload',
            'template' => 'image-form.html.php',
        ];
    }

    public function store()
    {
        $store = isset($_POST['id']) ? false : true;
        if ($store || isMaster()) {
            $imageInformation = $_FILES['image'];
            $imageValidator = new ImageValidator($_POST, $imageInformation, $store);

            $data = $imageValidator->getInput();

            store(self::ORIGINAL_TABLE, $data);

            return [
                'title' => 'Image Upload',
                'template' => 'image-form.html.php',
                'variables' => [
                    'messages' => $imageValidator->getMessages(),
                    'imageLocation' => $data[':image'] ?? "",
                ],
            ];
        } else {
            http_response_code(403);
            header('Location: ?route=images');
        }
    }
}