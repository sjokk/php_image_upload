<?php

namespace Images\Controllers;

use Images\Classes\LoginValidator;
use Images\Generics\DangerAlert;
use Images\Generics\SuccessAlert;

class LoginController
{
    public function show()
    {
        return [
            'title' => 'Login',
            'template' => 'login-form.html.php',
        ];
    }

    public function validate()
    {
        $loginValidator = new LoginValidator($_POST);

        if ($loginValidator->isValid()) {
            $_SESSION['master'] = true;
            $route = isset($_SESSION['previous_url']) ? $_SESSION['previous_url'] : '?route=images';
            header("Location: $route");
        }

        return [
            'title' => 'Login',
            'template' => 'login-form.html.php',
            'variables' => [
                'messages' => $loginValidator->getMessages(),
            ],
        ];
    }

    public function logout()
    {
        session_destroy();
        header('Location: ?route=images');
    }
}