Important is to set up two environment variables.
For validation, you can set: app_user, and app_user_password in your environment variables.
These will be used for now to validate a master/admin user.

The master user is important since it is responsible for validating newly uploaded images. The user can
do this under the new uploads tab. Here the user can click an image, verify it immediately, edit the 
details or delete the image by the click of a button.

The delete button is more so responsible for putting an image in quarantine, such that someone else
could delete the image from drive and after that the record in the database.

If an image is marked as mature, only the master user for now has this overview available.

Any regular visitor can see the non mature images that are verified at the home page, while 
any user is also allowed to upload images for now.