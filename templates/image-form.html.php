<?php if(isset($messages)): ?>
    <?php foreach($messages as $message): ?>
        <?= $message->output() ?>
    <?php endforeach; ?>
<?php endif; ?>

<form style="width: 50%;" class="container" method="POST" enctype="multipart/form-data">
    <?php if(isset($image)): ?>
        <input type="hidden" name="id" value="<?= $id ?>">
        <input type="hidden" name="image" value="<?= $image['image'] ?>">
    <?php endif; ?>
    <div class="field">
        <label for="title" class="label">Title</label>
        <?php if(isset($id)): ?>
            <input id="title" name="title" type="text" class="input" value="<?= $image['title'] ?>">
        <?php else: ?>
            <input id="title" name="title" type="text" class="input">
        <?php endif; ?>
    </div>
    <div class="file has-name field">
        <label class="file-label">
            <input class="file-input" type="file" name="image" id="image-file" accept="image/*">
            <span class="file-cta">
                <span class="file-icon">
                    <i class="fas fa-upload"></i>
                </span>
                <span class="file-label">
                    Choose an image...
                </span>
            </span>
            <span id="file-name" class="file-name">
            </span>
        </label>
    </div>
    <div class="field">
    <label class="checkbox">
        <?php if(isset($id)): ?>
            <?php if($image['hidden'] == "1"): ?>
                <input type="checkbox" name="hidden" checked>
            <?php else: ?>
                <input type="checkbox" name="hidden">
            <?php endif; ?>
        <?php else: ?>
            <input type="checkbox" name="hidden">
        <?php endif; ?>
        Mature Content
    </label>
    </div>
    <div class="field">
        <p class="control">
            <?php if(!isset($id)): ?>
                <input type="submit" value="Upload" class="button is-success">
            <?php else: ?>
                <input type="submit" value="Update" class="button is-success">
            <?php endif; ?>
        </p>
    </div>
</form>

<?php if(isset($imageLocation)): ?>
    <div class="container" style="padding: 0.25rem; width: 50%">
        <figure class="image">
            <img src="<?= $imageLocation ?>" alt="">
        </figure>
    </div>
<?php endif; ?>