<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.css.map">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.min.css">
    <title><?= $title ?></title>
</head>
<body>
    <nav class="navbar" role="navigation" aria-label="main navigation">
    <div class="navbar-brand">
        <a class="navbar-item" href="">
            <img src="" width="112" height="28">
        </a>
    </div>
    <div id="navbarBasicExample" class="navbar-menu">
        <div class="navbar-start">
            <a href="?route=images" class="navbar-item">
                Home
            </a>         
            <a href="?route=images/create" class="navbar-item">
                Upload
            </a>
            <?php if(isset($_SESSION['master'])): ?>
                <a href="?route=images/nsfw" class="navbar-item">
                    Mature
                </a>
                <a href="?route=images/verify" class="navbar-item">
                    New Uploads
                    <?php if(isset($_SESSION['new_upload']) && $_SESSION['new_upload'] == true): ?>
                        <i class="fa fa-exclamation-circle" style="margin-left: 2%; color: red;"></i>
                    <?php endif; ?>
                </a>
                <a href="?route=images/deleted" class="navbar-item">
                    Deleted Images
                </a>
            <?php endif; ?>
        </div>
    </div>
    <div class="navbar-end">
        <div class="navbar-item">
            <div class="buttons">
                <?php if(!isset($_SESSION['master'])): ?>
                    <a href="?route=images/login" class="button is-primary">
                        Log in
                    </a>
                <?php else: ?>
                    <a href="?route=images/logout" class="button is-primary">
                        Log out
                    </a>
                <?php endif; ?>
            </div>
        </div>
        </div>
    </div>
    </nav>
    <section class="section">
        <div class="container">
            <?= $output ?>
        </div>
    </section>

    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
    <script>
        var file = document.querySelector("#image-file");
        file.onchange = function() {
            if(file.files.length > 0)
            {
                document.querySelector('#file-name').innerHTML = file.files[0].name;
            }
        };
    </script>

</body>
</html>