<div class="content is-medium" style="text-align: center;">
    <h1>
        <?= $image['title'] ?>
    </h1>
    <p>
        Uploaded at: <?= $image['uploaded_at'] ?>
    </p>
    <?php if(isMaster()): ?>
        <p>
            This Content is Marked As 
        <?php if($image['hidden'] == '0'): ?>
            Not
        <?php endif; ?>
            Mature
        </p>
    <?php endif; ?>
</div>
<div class="columns">
    <div class="column">
        <?php if(!empty($previous)): ?>
            <a class="button is-light" href="<?= "?route=images/view&id={$previous['id']}" ?>" style="display: inline-block; width: 100%; height: 100%; text-align: center;">Previous</a>
        <?php endif; ?>
    </div>
    <div class="column is-three-quarters" style="text-align: center;">
    
    <?php if(isMaster()): ?>
        <?php if($image['verified'] == '0'): ?>
            <form action="?route=images/verify" method="POST" style="display: inline-block">
                <input type="hidden" name="next" value="<?= $next['id'] ?? null ?>">
                <input type="hidden" name="id" value="<?= $image['id'] ?>">
                <button class="button is-success is-focused">Approve Image</button>
            </form>
        <?php endif; ?>
    <?php endif;?>

        <a href="<?= $image['image'] ?>">
            <div class="container" style="width: 60%">
                <figure class="image">
                    <img src="<?= $image['image'] ?>">
                </figure>
            </div>
        </a>
    
    <?php if(isMaster()): ?>
        <a href="?route=images/edit&id=<?= $image['id'] ?>" class="button is-warning is-focused">Edit Details</a>

        <form action="?route=images/delete" method="POST" style="display: inline-block">
            <input type="hidden" name="next" value="<?= $next['id'] ?>">
            <input type="hidden" name="id" value="<?= $image['id'] ?>">
            <button class="button is-danger is-focused">Delete</button>
        </form>
    <?php endif; ?>
    
    </div>
    <div class="column">
        <?php if(!empty($next)): ?>
            <a class="button is-light" href="<?= "?route=images/view&id={$next['id']}" ?>" style="display: inline-block; width: 100%; height: 100%; text-align: center;">Next</a>
        <?php endif; ?>
    </div>
</div>