<?php if(isset($messages)): ?>
    <?php foreach($messages as $message): ?>
        <?= $message->output() ?>
    <?php endforeach; ?>
<?php endif; ?>

<form class="container" style="width: 30%" method="POST">
    <div class="field">
        <label for="username" class="label">Username</label>
        <input id="username" name="username" type="text" class="input">
    </div>
    <div class="field">
        <label for="password" class="label">Password</label>
        <input id="password" name="password" type="password" class="input">
    </div>
    <div class="field">
        <p class="control">
            <input type="submit" value="Login" class="button is-success">
        </p>
    </div>
</form>