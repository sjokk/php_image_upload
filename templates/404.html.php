<div class="tile is-ancestor container">
    <div class="tile is-parent">
    <article class="tile is-child notification is-danger">
        <p class="title">404 - Not Found</p>
        <p class="subtitle">On Your Behalf Nothing Was Found.</p>
        <div class="content level">
            <figure class="level-item image is-128x128">
            </figure>
            <figure class="level-item image is-128x128">
                <img style="width: auto;" src="functionality/anime-girl-confused-png-transparent.png">
            </figure>
        </div>
    </article>
    </div>
</div>