<?php for($i = 0; $i < count($images); $i++): ?>
    <?php if($i === 0): ?>
        <div class="columns">
    <?php elseif($i % 4 === 0): ?>
        </div>
        <div class="columns">
    <?php endif; ?>
    <div class="is-one-quarter" style="padding: 0.25rem; width: 25%">
        <a href="?route=images/view&id=<?= $images[$i]['id'] ?>">
            <figure class="image">
                <img class="is-rounded" src="<?= $images[$i]['image'] ?>" alt="<?= $images[$i]['title'] ?>">
            </figure>
        </a>
    </div>
<?php endfor; ?>